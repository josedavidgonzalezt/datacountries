import React, {useState,Fragment} from 'react';

const Formulario = ({pais,guardarPais,guardarConsultar}) => {

    //extraemos los datos
    const {nombre}=pais;
   const [error, actualizarError]=useState(false);
   

    //actualiza el state
    const handleChange=e=>{
        guardarPais({
            ...pais,
            [e.target.name] : e.target.value
        })
    }

    //al hacer submit
    const consultarPais=e=>{
        e.preventDefault();
        if(nombre.trim()===''||nombre.trim()==='a'||nombre.trim()=== 'b'|| nombre.trim()==='c'|| nombre.trim()==='d'|| nombre.trim()==='e'|| nombre.trim()==='f'|| nombre.trim()==='g'||nombre.trim()=== 'h'||nombre.trim()=== 'i'|| nombre.trim()==='j'||nombre.trim()=== 'k'|| nombre.trim()==='l'||nombre.trim()=== 'm'|| nombre.trim()==='n'||nombre.trim()=== 'ñ'||nombre.trim()=== 'o'||nombre.trim()=== 'p'|| nombre.trim()==='q'|| nombre.trim()==='r'|| nombre.trim()==='s'||nombre.trim()=== 't'||nombre.trim()=== 'u'||nombre.trim()=== 'v'|| nombre.trim()==='w'|| nombre.trim()==='x'||nombre.trim()=== 'y'||nombre.trim()=== 'z'|| nombre.trim()==='ch'){
            actualizarError(true);
            return;
        }
        
        guardarConsultar(true);
        actualizarError(false);
        
    }
    

    return ( 
        <Fragment>
        <form
            onSubmit={consultarPais}
        >
            {error ?<p className="alert alert-danger">You have not written a country!</p> :null}
            <label className="text-white" >Write a country</label>
            <input
            className="input-text"
            type="text"
            name="nombre"
            value={nombre}
            onChange={handleChange}
            />
            <input
            value='Search'
            type="submit"
            className="btn btn-primary boton"
            />
        </form>
        </Fragment>
     );
}
 
export default Formulario ;