import React from 'react';

const Header = () => {
    return ( 
        <header className="header text-center">
          <h1 className="titulo-header">Countries</h1>
      </header>
     );
}
 
export default Header;