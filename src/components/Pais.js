import React,{Fragment} from 'react';

const Pais = ({resultado,actualizarMostrar,guardarPais}) => {

    const volver=()=>{
        actualizarMostrar(false);
        guardarPais({
            nombre:''
        })
    }

    const {nativeName,name, flag, population, capital, currencies,languages,region,subregion} =resultado;
console.log(currencies[0],languages[0]);
    return ( 
        <Fragment>
        <div className="col-md-6 text-white caja-paises">
            <div className="header-card titulo-header">
            <h3 className="efecto-espejo">{nativeName}</h3>
            <p>{name}</p>
            </div>
            <div className="card-body">
                <img className="bandera parpadea" src={flag} alt="bandera"/>
                <h1 className="card-title pricing-card-title">{capital}</h1>
                <p className="poblacion text-muted">Capital</p>
                <ul className="list-unstyled mt-3 mb-4">
                    <li><span className="text-muted">Population: </span><span className="texto-c">{population}</span></li>
                    <li><span className="text-muted">Lenguaje: </span><span className="texto-c">{languages[0].name}</span></li>
                    <li><span className="text-muted">Official Coin: </span><span className="texto-c">{currencies[0].name}</span></li>
                    <li><span className="text-muted">Region: </span><span className="texto-c">{region}</span></li>
                    <li><span className="text-muted">Subregion: </span><span className="texto-c">{subregion}</span></li>
                </ul>
                <button 
                    type="button" 
                    className="btn btn-lg btn-outline-primary" 
                    onClick={()=>volver()}>Back</button>
            </div>
        </div>
        </Fragment>
     );
}
 
export default Pais;