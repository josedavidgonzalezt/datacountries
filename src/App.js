import React,{Fragment, useState, useEffect} from 'react';
import Header from './components/Header';
import Formulario from './components/Formulario';
import Pais from './components/Pais';

function App() {
//estate que mostra el contendor de de los paises
const [mostrar,actualizarMostrar]=useState(false);
const [consultar, guardarConsultar]=useState(false);
const [resultado,guardarResultado]=useState([]);
const [pais, guardarPais]= useState({
  nombre:''
});

//State de error
const [error,actualizarError]=useState(false);

const {nombre}=pais;

useEffect(() => {
  const consultarAPI=async()=>{

    if (consultar){
      const url=`https://restcountries.eu/rest/v2/name/${nombre}`;
      const respuesta= await fetch(url);
      const resultado = await respuesta.json();
      guardarConsultar(false)
      if(resultado.status===404){
        actualizarError(true);
        console.log('hay un error');
        return
      }
      else {
        actualizarError(false);
        guardarResultado(resultado[0]);
        actualizarMostrar(true);
        
    
      }
    }
  }
  consultarAPI();
}, [consultar,nombre])

  return (
    <Fragment>
      <Header/>
        <div className="container">
          {mostrar 
            ? <div className="container">
                <div className="card-deck text-center centrar">
                  <Pais 
                    resultado={resultado}
                    actualizarMostrar={actualizarMostrar}
                    guardarPais={guardarPais}
                  />
                </div>
              </div>
            :<div className="row centrar">
                <div className="formulario col-md-6">
                  <Formulario
                    pais={pais}
                    guardarPais={guardarPais}
                    guardarConsultar={guardarConsultar}
                    
                  />
                  {error ? <p className="text-white">Check that the name is spelled correctly<br/><span>Try english name</span></p>: null}
                </div>
                </div>
          }
        </div>
    </Fragment>
  );
}


export default App;
